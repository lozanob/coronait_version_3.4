/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */


//global  
 const $ = require('jquery');
 global.$ = global.jQuery = $;

import '../css/small-business.css';
import '../css/app.css';
import '../images/contact.jpg';
import '../images/support.jpg';
import '../images/maintenance.jpg';

var Slick = require('slick-carousel');
require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.bundle.js');





$(document).ready(function() {

	showMenu(); 

	function showMenu(){
	   $('.li-service').hover(function() {
			$('ul.menucache').addClass('showMenu'); 
		},function () {
	  	$('ul.menucache').removeClass('showMenu'); 
		});
	}



		layoutCarousel(); 

		function layoutCarousel(){
	         $('.single-item').slick();
		}







  });
 


// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';


