(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["resume"],{

/***/ "./assets/css/resume.css":
/*!*******************************!*\
  !*** ./assets/css/resume.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/images/profile.jpg":
/*!***********************************!*\
  !*** ./assets/images/profile.jpg ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/build/images/profile.df0bc390.jpg");

/***/ }),

/***/ "./assets/js/resume.js":
/*!*****************************!*\
  !*** ./assets/js/resume.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _css_resume_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../css/resume.css */ "./assets/css/resume.css");
/* harmony import */ var _css_resume_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_css_resume_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _images_profile_jpg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../images/profile.jpg */ "./assets/images/profile.jpg");




/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");

global.$ = global.jQuery = $;



__webpack_require__(/*! bootstrap/dist/css/bootstrap.css */ "./node_modules/bootstrap/dist/css/bootstrap.css");

__webpack_require__(/*! bootstrap/dist/js/bootstrap.bundle.js */ "./node_modules/bootstrap/dist/js/bootstrap.bundle.js");

__webpack_require__(/*! jquery.easing/jquery.easing.js */ "./node_modules/jquery.easing/jquery.easing.js");

__webpack_require__(/*! @fortawesome/fontawesome-free/css/all.css */ "./node_modules/@fortawesome/fontawesome-free/css/all.css");

$(document).ready(function () {
  resume();

  function resume() {
    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000, "easeInOutExpo");
          return false;
        }
      }
    }); // Closes responsive menu when a scroll trigger link is clicked

    $('.js-scroll-trigger').click(function () {
      $('.navbar-collapse').collapse('hide');
    }); // Activate scrollspy to add active class to navbar items on scroll

    $('body').scrollspy({
      target: '#sideNav'
    });
  }

  ;
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ })

},[["./assets/js/resume.js","runtime","vendors~app~home~resume","vendors~home~resume","vendors~resume"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL3Jlc3VtZS5jc3MiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2ltYWdlcy9wcm9maWxlLmpwZyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvcmVzdW1lLmpzIl0sIm5hbWVzIjpbIiQiLCJyZXF1aXJlIiwiZ2xvYmFsIiwialF1ZXJ5IiwiZG9jdW1lbnQiLCJyZWFkeSIsInJlc3VtZSIsImNsaWNrIiwibG9jYXRpb24iLCJwYXRobmFtZSIsInJlcGxhY2UiLCJob3N0bmFtZSIsInRhcmdldCIsImhhc2giLCJsZW5ndGgiLCJzbGljZSIsImFuaW1hdGUiLCJzY3JvbGxUb3AiLCJvZmZzZXQiLCJ0b3AiLCJjb2xsYXBzZSIsInNjcm9sbHNweSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsdUM7Ozs7Ozs7Ozs7OztBQ0FBO0FBQWUsbUdBQW9DLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRW5EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUlBLElBQU1BLENBQUMsR0FBR0MsbUJBQU8sQ0FBQyxvREFBRCxDQUFqQjs7QUFDQUMsTUFBTSxDQUFDRixDQUFQLEdBQVdFLE1BQU0sQ0FBQ0MsTUFBUCxHQUFnQkgsQ0FBM0I7QUFJQTtBQUNBOztBQUdBQyxtQkFBTyxDQUFDLHlGQUFELENBQVA7O0FBQ0FBLG1CQUFPLENBQUMsbUdBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQyxxRkFBRCxDQUFQOztBQUNBQSxtQkFBTyxDQUFDLDJHQUFELENBQVA7O0FBS0FELENBQUMsQ0FBQ0ksUUFBRCxDQUFELENBQVlDLEtBQVosQ0FBa0IsWUFBVztBQUV2QkMsUUFBTTs7QUFHVixXQUFVQSxNQUFWLEdBQW1CO0FBRWY7QUFDRU4sS0FBQyxDQUFDLGdEQUFELENBQUQsQ0FBb0RPLEtBQXBELENBQTBELFlBQVc7QUFDbkUsVUFBSUMsUUFBUSxDQUFDQyxRQUFULENBQWtCQyxPQUFsQixDQUEwQixLQUExQixFQUFpQyxFQUFqQyxLQUF3QyxLQUFLRCxRQUFMLENBQWNDLE9BQWQsQ0FBc0IsS0FBdEIsRUFBNkIsRUFBN0IsQ0FBeEMsSUFBNEVGLFFBQVEsQ0FBQ0csUUFBVCxJQUFxQixLQUFLQSxRQUExRyxFQUFvSDtBQUNsSCxZQUFJQyxNQUFNLEdBQUdaLENBQUMsQ0FBQyxLQUFLYSxJQUFOLENBQWQ7QUFDQUQsY0FBTSxHQUFHQSxNQUFNLENBQUNFLE1BQVAsR0FBZ0JGLE1BQWhCLEdBQXlCWixDQUFDLENBQUMsV0FBVyxLQUFLYSxJQUFMLENBQVVFLEtBQVYsQ0FBZ0IsQ0FBaEIsQ0FBWCxHQUFnQyxHQUFqQyxDQUFuQzs7QUFDQSxZQUFJSCxNQUFNLENBQUNFLE1BQVgsRUFBbUI7QUFDakJkLFdBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0JnQixPQUFoQixDQUF3QjtBQUN0QkMscUJBQVMsRUFBR0wsTUFBTSxDQUFDTSxNQUFQLEdBQWdCQztBQUROLFdBQXhCLEVBRUcsSUFGSCxFQUVTLGVBRlQ7QUFHQSxpQkFBTyxLQUFQO0FBQ0Q7QUFDRjtBQUNGLEtBWEQsRUFIYSxDQWdCWDs7QUFDQW5CLEtBQUMsQ0FBQyxvQkFBRCxDQUFELENBQXdCTyxLQUF4QixDQUE4QixZQUFXO0FBQ3ZDUCxPQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQm9CLFFBQXRCLENBQStCLE1BQS9CO0FBQ0QsS0FGRCxFQWpCVyxDQXFCbkI7O0FBQ0FwQixLQUFDLENBQUMsTUFBRCxDQUFELENBQVVxQixTQUFWLENBQW9CO0FBQ2xCVCxZQUFNLEVBQUU7QUFEVSxLQUFwQjtBQUtEOztBQUFBO0FBRUUsQ0FsQ0gsRSIsImZpbGUiOiJyZXN1bWUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iLCJleHBvcnQgZGVmYXVsdCBcIi9idWlsZC9pbWFnZXMvcHJvZmlsZS5kZjBiYzM5MC5qcGdcIjsiLCJcblxuLypcbiAqIFdlbGNvbWUgdG8geW91ciBhcHAncyBtYWluIEphdmFTY3JpcHQgZmlsZSFcbiAqXG4gKiBXZSByZWNvbW1lbmQgaW5jbHVkaW5nIHRoZSBidWlsdCB2ZXJzaW9uIG9mIHRoaXMgSmF2YVNjcmlwdCBmaWxlXG4gKiAoYW5kIGl0cyBDU1MgZmlsZSkgaW4geW91ciBiYXNlIGxheW91dCAoYmFzZS5odG1sLnR3aWcpLlxuICovXG5cblxuXG5jb25zdCAkID0gcmVxdWlyZSgnanF1ZXJ5Jyk7XG5nbG9iYWwuJCA9IGdsb2JhbC5qUXVlcnkgPSAkO1xuXG5cblxuaW1wb3J0ICcuLi9jc3MvcmVzdW1lLmNzcyc7XG5pbXBvcnQgJy4uL2ltYWdlcy9wcm9maWxlLmpwZyc7XG5cblxucmVxdWlyZSgnYm9vdHN0cmFwL2Rpc3QvY3NzL2Jvb3RzdHJhcC5jc3MnKTtcbnJlcXVpcmUoJ2Jvb3RzdHJhcC9kaXN0L2pzL2Jvb3RzdHJhcC5idW5kbGUuanMnKTtcbnJlcXVpcmUoJ2pxdWVyeS5lYXNpbmcvanF1ZXJ5LmVhc2luZy5qcycpO1xucmVxdWlyZSgnQGZvcnRhd2Vzb21lL2ZvbnRhd2Vzb21lLWZyZWUvY3NzL2FsbC5jc3MnKTtcblxuXG5cblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG5cbiAgICAgIHJlc3VtZSgpOyBcblxuXG4gIGZ1bmN0aW9uICByZXN1bWUoKSB7XG5cbiAgICAgIC8vIFNtb290aCBzY3JvbGxpbmcgdXNpbmcgalF1ZXJ5IGVhc2luZ1xuICAgICAgICAkKCdhLmpzLXNjcm9sbC10cmlnZ2VyW2hyZWYqPVwiI1wiXTpub3QoW2hyZWY9XCIjXCJdKScpLmNsaWNrKGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmIChsb2NhdGlvbi5wYXRobmFtZS5yZXBsYWNlKC9eXFwvLywgJycpID09IHRoaXMucGF0aG5hbWUucmVwbGFjZSgvXlxcLy8sICcnKSAmJiBsb2NhdGlvbi5ob3N0bmFtZSA9PSB0aGlzLmhvc3RuYW1lKSB7XG4gICAgICAgICAgICB2YXIgdGFyZ2V0ID0gJCh0aGlzLmhhc2gpO1xuICAgICAgICAgICAgdGFyZ2V0ID0gdGFyZ2V0Lmxlbmd0aCA/IHRhcmdldCA6ICQoJ1tuYW1lPScgKyB0aGlzLmhhc2guc2xpY2UoMSkgKyAnXScpO1xuICAgICAgICAgICAgaWYgKHRhcmdldC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgJCgnaHRtbCwgYm9keScpLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgIHNjcm9sbFRvcDogKHRhcmdldC5vZmZzZXQoKS50b3ApXG4gICAgICAgICAgICAgIH0sIDEwMDAsIFwiZWFzZUluT3V0RXhwb1wiKTtcbiAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgICAvLyBDbG9zZXMgcmVzcG9uc2l2ZSBtZW51IHdoZW4gYSBzY3JvbGwgdHJpZ2dlciBsaW5rIGlzIGNsaWNrZWRcbiAgICAgICAgICAkKCcuanMtc2Nyb2xsLXRyaWdnZXInKS5jbGljayhmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICQoJy5uYXZiYXItY29sbGFwc2UnKS5jb2xsYXBzZSgnaGlkZScpO1xuICAgICAgICAgIH0pO1xuXG4gIC8vIEFjdGl2YXRlIHNjcm9sbHNweSB0byBhZGQgYWN0aXZlIGNsYXNzIHRvIG5hdmJhciBpdGVtcyBvbiBzY3JvbGxcbiAgJCgnYm9keScpLnNjcm9sbHNweSh7XG4gICAgdGFyZ2V0OiAnI3NpZGVOYXYnXG4gIH0pO1xuXG4gIFxufTtcblxuICB9KTtcbiAiXSwic291cmNlUm9vdCI6IiJ9