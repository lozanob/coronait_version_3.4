(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home"],{

/***/ "./assets/css/coming-soon.css":
/*!************************************!*\
  !*** ./assets/css/coming-soon.css ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/css/home.css":
/*!*****************************!*\
  !*** ./assets/css/home.css ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/images/Node.png":
/*!********************************!*\
  !*** ./assets/images/Node.png ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/build/images/Node.8893a109.png");

/***/ }),

/***/ "./assets/images/bg.mp4":
/*!******************************!*\
  !*** ./assets/images/bg.mp4 ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "861fe25c2756d7c4cb05af50a80ee476.mp4");

/***/ }),

/***/ "./assets/images/design-pattern.jpg":
/*!******************************************!*\
  !*** ./assets/images/design-pattern.jpg ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/build/images/design-pattern.69f2a08a.jpg");

/***/ }),

/***/ "./assets/images/methode_agile.jpg":
/*!*****************************************!*\
  !*** ./assets/images/methode_agile.jpg ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/build/images/methode_agile.61326842.jpg");

/***/ }),

/***/ "./assets/images/symfony-brands.svg":
/*!******************************************!*\
  !*** ./assets/images/symfony-brands.svg ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/build/images/symfony-brands.5927d169.svg");

/***/ }),

/***/ "./assets/js/home.js":
/*!***************************!*\
  !*** ./assets/js/home.js ***!
  \***************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var _css_coming_soon_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../css/coming-soon.css */ "./assets/css/coming-soon.css");
/* harmony import */ var _css_coming_soon_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_css_coming_soon_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _css_home_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../css/home.css */ "./assets/css/home.css");
/* harmony import */ var _css_home_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_css_home_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _images_bg_mp4__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../images/bg.mp4 */ "./assets/images/bg.mp4");
/* harmony import */ var _images_symfony_brands_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../images/symfony-brands.svg */ "./assets/images/symfony-brands.svg");
/* harmony import */ var _images_methode_agile_jpg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../images/methode_agile.jpg */ "./assets/images/methode_agile.jpg");
/* harmony import */ var _images_design_pattern_jpg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../images/design-pattern.jpg */ "./assets/images/design-pattern.jpg");
/* harmony import */ var _images_Node_png__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../images/Node.png */ "./assets/images/Node.png");
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
//global  
var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");

global.$ = global.jQuery = $;








__webpack_require__(/*! bootstrap/dist/css/bootstrap.css */ "./node_modules/bootstrap/dist/css/bootstrap.css");

__webpack_require__(/*! bootstrap/dist/js/bootstrap.bundle.js */ "./node_modules/bootstrap/dist/js/bootstrap.bundle.js");

__webpack_require__(/*! @fortawesome/fontawesome-free/css/all.css */ "./node_modules/@fortawesome/fontawesome-free/css/all.css");

$(document).ready(function () {
  $('body div.masthead-bg').css('background-color', '#ff7945');
  $('body div.overlay').css('background-color', '#cd955757');
}); // Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ })

},[["./assets/js/home.js","runtime","vendors~app~home~resume","vendors~home~resume"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL2NvbWluZy1zb29uLmNzcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL2hvbWUuY3NzIiwid2VicGFjazovLy8uL2Fzc2V0cy9pbWFnZXMvTm9kZS5wbmciLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2ltYWdlcy9iZy5tcDQiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2ltYWdlcy9kZXNpZ24tcGF0dGVybi5qcGciLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2ltYWdlcy9tZXRob2RlX2FnaWxlLmpwZyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvaW1hZ2VzL3N5bWZvbnktYnJhbmRzLnN2ZyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvaG9tZS5qcyJdLCJuYW1lcyI6WyIkIiwicmVxdWlyZSIsImdsb2JhbCIsImpRdWVyeSIsImRvY3VtZW50IiwicmVhZHkiLCJjc3MiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLHVDOzs7Ozs7Ozs7OztBQ0FBLHVDOzs7Ozs7Ozs7Ozs7QUNBQTtBQUFlLGdHQUFpQyxFOzs7Ozs7Ozs7Ozs7QUNBaEQ7QUFBZSxvRkFBdUIseUNBQXlDLEU7Ozs7Ozs7Ozs7OztBQ0EvRTtBQUFlLDBHQUEyQyxFOzs7Ozs7Ozs7Ozs7QUNBMUQ7QUFBZSx5R0FBMEMsRTs7Ozs7Ozs7Ozs7O0FDQXpEO0FBQWUsMEdBQTJDLEU7Ozs7Ozs7Ozs7OztBQ0ExRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0MsSUFBTUEsQ0FBQyxHQUFHQyxtQkFBTyxDQUFDLG9EQUFELENBQWpCOztBQUNBQyxNQUFNLENBQUNGLENBQVAsR0FBV0UsTUFBTSxDQUFDQyxNQUFQLEdBQWdCSCxDQUEzQjtBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUdBQyxtQkFBTyxDQUFDLHlGQUFELENBQVA7O0FBQ0FBLG1CQUFPLENBQUMsbUdBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQywyR0FBRCxDQUFQOztBQUtBRCxDQUFDLENBQUNJLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQVc7QUFHeEJMLEdBQUMsQ0FBQyxzQkFBRCxDQUFELENBQTBCTSxHQUExQixDQUE4QixrQkFBOUIsRUFBaUQsU0FBakQ7QUFDQU4sR0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0JNLEdBQXRCLENBQTBCLGtCQUExQixFQUE2QyxXQUE3QztBQUVGLENBTkgsRSxDQVVBO0FBQ0EsMEIiLCJmaWxlIjoiaG9tZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiIsImV4cG9ydCBkZWZhdWx0IFwiL2J1aWxkL2ltYWdlcy9Ob2RlLjg4OTNhMTA5LnBuZ1wiOyIsImV4cG9ydCBkZWZhdWx0IF9fd2VicGFja19wdWJsaWNfcGF0aF9fICsgXCI4NjFmZTI1YzI3NTZkN2M0Y2IwNWFmNTBhODBlZTQ3Ni5tcDRcIjsiLCJleHBvcnQgZGVmYXVsdCBcIi9idWlsZC9pbWFnZXMvZGVzaWduLXBhdHRlcm4uNjlmMmEwOGEuanBnXCI7IiwiZXhwb3J0IGRlZmF1bHQgXCIvYnVpbGQvaW1hZ2VzL21ldGhvZGVfYWdpbGUuNjEzMjY4NDIuanBnXCI7IiwiZXhwb3J0IGRlZmF1bHQgXCIvYnVpbGQvaW1hZ2VzL3N5bWZvbnktYnJhbmRzLjU5MjdkMTY5LnN2Z1wiOyIsIi8qXG4gKiBXZWxjb21lIHRvIHlvdXIgYXBwJ3MgbWFpbiBKYXZhU2NyaXB0IGZpbGUhXG4gKlxuICogV2UgcmVjb21tZW5kIGluY2x1ZGluZyB0aGUgYnVpbHQgdmVyc2lvbiBvZiB0aGlzIEphdmFTY3JpcHQgZmlsZVxuICogKGFuZCBpdHMgQ1NTIGZpbGUpIGluIHlvdXIgYmFzZSBsYXlvdXQgKGJhc2UuaHRtbC50d2lnKS5cbiAqL1xuXG5cbi8vZ2xvYmFsICBcbiBjb25zdCAkID0gcmVxdWlyZSgnanF1ZXJ5Jyk7XG4gZ2xvYmFsLiQgPSBnbG9iYWwualF1ZXJ5ID0gJDtcblxuaW1wb3J0ICcuLi9jc3MvY29taW5nLXNvb24uY3NzJztcbmltcG9ydCAnLi4vY3NzL2hvbWUuY3NzJztcbmltcG9ydCAnLi4vaW1hZ2VzL2JnLm1wNCc7XG5pbXBvcnQgJy4uL2ltYWdlcy9zeW1mb255LWJyYW5kcy5zdmcnO1xuaW1wb3J0ICcuLi9pbWFnZXMvbWV0aG9kZV9hZ2lsZS5qcGcnO1xuaW1wb3J0ICcuLi9pbWFnZXMvZGVzaWduLXBhdHRlcm4uanBnJztcbmltcG9ydCAnLi4vaW1hZ2VzL05vZGUucG5nJztcblxuXG5yZXF1aXJlKCdib290c3RyYXAvZGlzdC9jc3MvYm9vdHN0cmFwLmNzcycpO1xucmVxdWlyZSgnYm9vdHN0cmFwL2Rpc3QvanMvYm9vdHN0cmFwLmJ1bmRsZS5qcycpO1xucmVxdWlyZSgnQGZvcnRhd2Vzb21lL2ZvbnRhd2Vzb21lLWZyZWUvY3NzL2FsbC5jc3MnKTtcblxuXG5cblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG5cblxuICAgICAkKCdib2R5IGRpdi5tYXN0aGVhZC1iZycpLmNzcygnYmFja2dyb3VuZC1jb2xvcicsJyNmZjc5NDUnKTtcbiAgICAgJCgnYm9keSBkaXYub3ZlcmxheScpLmNzcygnYmFja2dyb3VuZC1jb2xvcicsJyNjZDk1NTc1NycpO1xuXG4gIH0pO1xuIFxuXG5cbi8vIE5lZWQgalF1ZXJ5PyBJbnN0YWxsIGl0IHdpdGggXCJ5YXJuIGFkZCBqcXVlcnlcIiwgdGhlbiB1bmNvbW1lbnQgdG8gaW1wb3J0IGl0LlxuLy8gaW1wb3J0ICQgZnJvbSAnanF1ZXJ5JztcblxuXG4iXSwic291cmNlUm9vdCI6IiJ9