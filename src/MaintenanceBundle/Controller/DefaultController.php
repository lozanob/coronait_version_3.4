<?php

namespace MaintenanceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{


     /**
     * @Route("/maintenance", name="maintenance")
     */
    public function indexAction()
    {
        //return $this->render('MaintenanceBundle:Default:index.html.twig');
                //return $this->render('SupportBundle:Default:index.html.twig');
           return $this->render('default/maintenance.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
           
    }
}
