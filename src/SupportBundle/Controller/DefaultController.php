<?php

namespace SupportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    
     /**
     * @Route("/support", name="support")
     */
    public function indexAction()
    {
        //return $this->render('SupportBundle:Default:index.html.twig');
           return $this->render('default/support.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);

    }
}
